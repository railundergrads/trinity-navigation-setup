# container from https://hub.docker.com/_/ros (melodic-robot-bionic version)
FROM ros:melodic-robot-bionic

RUN sudo apt-get update

RUN sudo apt-get -y install python3-pip ros-melodic-navigation ros-melodic-openslam-gmapping ros-melodic-rplidar-ros ros-melodic-hector-slam ros-melodic-sensor-msgs ros-melodic-tf ros-melodic-rviz curl ros-melodic-rqt ros-melodic-rqt-common-plugins ros-melodic-rqt-robot-plugins ros-melodic-image-geometry ros-melodic-gazebo-ros-pkgs ros-melodic-gazebo-ros-control

# Hack to get catkin_make to work, needs redoing

RUN mkdir -p trinity_navigation_workspace/src

RUN /bin/bash -c '. /opt/ros/melodic/setup.bash; cd trinity_navigation_workspace; catkin_make'
#Turtlebot commands can be removed later

RUN cd trinity_navigation_workspace/src && curl -sLf https://raw.githubusercontent.com/gaunthan/Turtlebot2-On-Melodic/master/install_all.sh | bash

RUN /bin/bash -c '. /opt/ros/melodic/setup.bash; cd trinity_navigation_workspace; catkin_make'
RUN echo "source /opt/ros/melodic/setup.bash" >> ~/.bashrc
# RUN /bin/bash -c "source ~/turtlebot_ws/devel/setup.bash"
# RUN echo "source ~/trinity_navigation_workspace.src/trinity_ros_navigation_stack/scripts/setup.bash" >> ~/.bashrc
#General ROS packages and packages we need

RUN cd trinity_navigation_workspace/src && \
    git clone https://gitlab.com/railundergrads/trinity_ros_navigation_stack.git


# RUN /bin/bash -c '. /opt/ros/melodic/setup.bash; cd trinity_navigation_workspace; catkin_make'
#Turtlebot packages for testing purposes
RUN sudo apt-get install -y ros-melodic-move-base* ros-melodic-map-server* ros-melodic-amcl* ros-melodic-navigation* ros-melodic-joy* 

# RUN catkin_make
